require 'spec_helper'
require 'psychobilly/google'

describe Psychobilly::Google do
  it "should have a VERSION constant" do
    subject.const_get('VERSION').should_not be_empty
  end
end
