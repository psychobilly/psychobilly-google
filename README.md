psychobilly-google
==================

gem template
------------
```bash
mine psychobilly-google --rubygems-tasks --bin --version=0.0.1 --markdown
```

Build
-----
```bash
VERSION=$(ruby lib/psychobilly/google/version.rb)
gem build psychobilly-google.gemspec
```

Install
-------
### via local
```bash
gem install psychobilly-google-${VERSION}.gem
```
# via remote
```bash
gem install psychobilly-google
```

Examples
--------
### googleのトレンドを取得する
```ruby
require 'rubygems' if RUBY_VERSION < "1.9"
require 'psychobilly/google'

Psychobilly::Google.getTrends.sort_by{rand}.each do |item|
  p item.to_s
end
```
