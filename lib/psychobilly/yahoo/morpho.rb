# -*- mode:ruby; coding:utf-8 -*-

require 'rexml/document'
require 'open-uri'

module Psychobilly
  module Yahoo

    module_function
    def morphologicalAnalysis(sentence)
      appid = 'BhHoLTexg66ymJ0736jfTwQjuj2DGDSWdv12rxquJiVF_AD6cniEKsDLGG0.xmEAwDun0UYP'
      api = 'http://jlp.yahooapis.jp/MAService/V1/parse'
      result = Array.new()
      body = open("#{api}?appid=#{appid}&results=ma&sentence="+URI.encode("#{sentence}"))
      doc = REXML::Document.new(body).elements['ResultSet/ma_result/word_list/']
      doc.elements.each('word') do |item|
        word = Hash.new()
        item.elements.each do |property|
          word["#{property.name}"] = property.text
        end
        result << word
      end
      #result.toutf8
      result
    end

  end
end

__END__
