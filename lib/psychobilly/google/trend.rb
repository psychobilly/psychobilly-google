# -*- mode:ruby; coding: utf-8 -*-

require 'feed-normalizer'
require 'ostruct'
require 'memcached'
#$cache = Memcached.new("localhost:11211")

TRENDS_RSS_URL = 'http://www.google.co.jp/trends/hottrends/atom/feed'

module Psychobilly
  module Google

    module_function

    def getTrends(url=TRENDS_RSS_URL, cache=false)
      r = []
      rss = FeedNormalizer::FeedNormalizer.parse(open(TRENDS_RSS_URL))
      rss.entries.map do |item|
        r << OpenStruct.new({:title => item.title, :url => item.url, :description => item.description})
      end
      r
    end

  end
end

if $0 == __FILE__
  Psychobilly::Google.getTrends.sort_by{rand}.each do |item|
    p item.to_s
  end
end
