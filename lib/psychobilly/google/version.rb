module Psychobilly
  module Google
    # psychobilly-google version
    VERSION = "0.0.1"
  end
end

if $0 == __FILE__
  puts Psychobilly::Google::VERSION
end
