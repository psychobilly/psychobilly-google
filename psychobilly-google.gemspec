# -*- mode:ruby; coding: utf-8 -*-

require File.expand_path('../lib/psychobilly/google/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name          = "psychobilly-google"
  gem.version       = Psychobilly::Google::VERSION
  gem.summary       = %q{use google service}
  gem.description   = %q{use google service}
  gem.license       = "MIT"
  gem.authors       = ["東洲斎 写楽"]
  gem.email         = "guitarwolf@hotmail.co.jp"
  gem.homepage      = "http://gems.psychobil.ly/gems/psychobilly-google"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_dependency 'feed-normalizer'
  gem.add_dependency 'ostruct2'
  gem.add_dependency 'geminabox'
  gem.add_dependency 'memcached' #, '~> 1.8.0'
  gem.add_dependency 'hatena-bookmark', '~> 0.1.2'

  gem.add_development_dependency 'rdoc', '~> 3.0'
  gem.add_development_dependency 'rspec', '~> 2.4'
  gem.add_development_dependency 'rubygems-tasks', '~> 0.2'
end
